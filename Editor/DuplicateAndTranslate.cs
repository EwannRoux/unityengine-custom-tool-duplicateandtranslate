using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class DuplicateAndTranslate : EditorWindow {
    /// <summary>
    /// Static reference to the window.
    /// </summary>
    static DuplicateAndTranslate window;

    /// <summary>
    /// Return true if the next use of the command must call a record.
    /// </summary>
    public static bool _nextCallMustRecord = true;

    static List<GameObject> LastDuplicatedElementsList = new List<GameObject>();

    /// <summary>
    /// Object used for movement recording
    /// </summary>
    static GameObject MainObjectOfSelection;

    /// <summary>
    /// Return true if current selection is equal to the previous duplicated object. Return false if they mismatch or if there is no selection.
    /// </summary>
    static bool SelectionIsValid {
        get {
            //Return false if size is different or selection is empty.
            if (LastDuplicatedElementsList.Count != Selection.gameObjects.Length || Selection.gameObjects.Length == 0) {
                return false;
            }

            //Copy the selected GameObjects in a local List.
            List<GameObject> CurrentlySelectedGameObjects = new List<GameObject>(Selection.gameObjects);

            //Sort this local list to be sure both list will have object in same order for the comparison, since Selection.gameObjects seem to use an inconsistent way to 
            //sort GameObjects.
            CurrentlySelectedGameObjects.Sort((obj1, obj2) => obj1.GetInstanceID().CompareTo(obj2.GetInstanceID()));

            //Compare each GameObject with its equivalent of the other list.
            for (int i = 0; i < LastDuplicatedElementsList.Count; i++) {
                //If two object don't match, return false
                if (LastDuplicatedElementsList[i] != CurrentlySelectedGameObjects[i]) {
                    return false;
                }
            }

            //if no mismatch have been detected, return true
            return true;
        }
    }


    static bool _isRecording;

    static Vector3 _positionAtStartOfRecord;


    //used for text displayed on window
    public static bool _displayDebug = false;
    public static bool _displayTutorial = false;
    static GUIStyle TextGreen = new GUIStyle(EditorStyles.boldLabel);
    static GUIStyle TextRed = new GUIStyle(EditorStyles.boldLabel);
    static GUIStyle TextBlue = new GUIStyle(EditorStyles.boldLabel);



    // Validated menu item. Init
    // Add a menu item named "Log Selected Transform Name" to MyMenu in the menu bar.
    // We use a second function to validate the menu item
    // so it will only be enabled if we have a transform selected.
    [MenuItem("CustomTools/Duplicate and Translate #&D")]
    static void CallDuplicateAndTranslate() {

        //If there is no window, create a new and preparte to record a translation.
        if (!window) {
            DislayWindow();
            _nextCallMustRecord = true;
        }


        //if selection is valid,and that a record is ongoing.
        //_nextCallMustRecord is set to false only when a record start
        //and set tu true on the first call and when the selection is lost / escape is pressed
        if (SelectionIsValid && _nextCallMustRecord == false) {
            //if there is a record
            if (_translateToApply != Vector3.zero) {
                DuplicateThenTranslate();
            }
        }
        else {
            //Duplicate then start recording.
            //Typically called on the first call, and the calls following a loss of the selection / escape key press.
            DuplicateThenStartRecord();
        }

    }

    // Validate the menu item defined by the function above.
    // The menu item will be disabled if this function returns false.
    [MenuItem("CustomTools/Duplicate and Translate #&D", true)]
    static bool ValidateCallDuplicateAndTranslate() {
        // Return false if no GameObject is selected.
        return Selection.gameObjects.Length != 0;
    }


    private static void DislayWindow() {
        //Create a new window and assign it to the reference.
        window = (DuplicateAndTranslate)GetWindow(typeof(DuplicateAndTranslate));
        //Display the window.
        window.Show();
        //Set its title.
        window.titleContent.text = "Duplicate and Translate";

        //Set the text styles
        TextRed.normal.textColor = Color.red;
        TextBlue.normal.textColor = Color.cyan;

        //Custom color because Color.green is not easy to view on the window
        TextGreen.normal.textColor = new Color(0f, 0.5f, 0f);
    }




    /// <summary>
    /// Called when the window gets keyboard focus. Always called when the window is created.
    /// </summary>
    private void OnFocus() {
        SceneView.lastActiveSceneView.autoRepaintOnSceneChange = true;

        //not used here but may be useful if future update use OnSceneGUI drawing
        // Remove delegate listener if it has previously
        // been assigned.
        SceneView.duringSceneGui -= this.OnSceneGUI;
        // Add (or re-add) the delegate.
        SceneView.duringSceneGui += this.OnSceneGUI;
    }

    private void OnDestroy() {
        // when the window is destroyed, remove the delegate
        // so that it will no longer do any drawing.
        SceneView.duringSceneGui -= this.OnSceneGUI;

        //repaint all
        SceneView.RepaintAll();

        //make sure that next Init will launch correctly
        window = null;
    }

    static Vector3 _translateToApply;
    /// <summary>
    /// Window informations displayed.
    /// </summary>
    void OnGUI() {
        //Read Event before drawing any GUI, bc some input may change info to display.
        Event e = Event.current;
        //switch because use of other event may be needed in a future update of the script
        switch (e.type) {
            case EventType.KeyDown:
                if (e.keyCode == KeyCode.Escape) {
                    //_nextCallMustRecord as true will force the next call of the MenuItem to start a new record.
                    _nextCallMustRecord = true;
                }
                break;
        }

        #region DefaultDisplay
        //Display the value of the recorded movement, selectable in case of need for the user to store the value
        EditorGUILayout.SelectableLabel("Translate: " + _translateToApply, EditorStyles.boldLabel);

        //if currently recording 
        if (_isRecording) {
            EditorGUILayout.LabelField("Currently Recording", TextRed);
        }
        //If will use the movement to translate 
        else if (SelectionIsValid && _nextCallMustRecord == false) {
            EditorGUILayout.LabelField("Use Recorded Translate", TextGreen);
        }
        else {
            EditorGUILayout.LabelField("Next Call Will start recording", TextBlue);
        }
        #endregion

        #region DisplayTutorial
        //draw a toggle to check/uncheck for displaying tutorial
        _displayTutorial = EditorGUILayout.Toggle("Display Tutorial", _displayTutorial);
        if (_displayTutorial) {
            if (!_nextCallMustRecord) {
                EditorGUILayout.LabelField("Select a gameobject.");
                EditorGUILayout.LabelField("Press Shift  + D to duplicate.");
                EditorGUILayout.LabelField("Move the gameobject without unselecting it.");
                EditorGUILayout.LabelField("The motion will be recorded.");
                EditorGUILayout.LabelField("This motion can be made more than one move.");
                EditorGUILayout.LabelField("Press Shift + Alt + D to duplicate and apply the motion.");
                EditorGUILayout.LabelField("This operation can be repeted indefinitively.");
                EditorGUILayout.LabelField("Unselect then select a game object again");
                EditorGUILayout.LabelField("and use the command to change the motion.");
                EditorGUILayout.LabelField("Press \"Escape\" to cancel the last record,");
                EditorGUILayout.LabelField("after that the net call will duplicate then");
                EditorGUILayout.LabelField("start a new record.");
            }
        }
        #endregion

        #region DisplayDebug
        //Draw a toggle to check/uncheck for displaying debug data
        _displayDebug = EditorGUILayout.Toggle("Display Debug Data", _displayDebug);
        if (_displayDebug) {
            EditorGUILayout.LabelField("Next Command Call Will Record: " + _nextCallMustRecord);
            if (!_nextCallMustRecord) {
                //Display the value of the original position of the main element of the selection, selectable in case of need for the user to store the value
                EditorGUILayout.SelectableLabel("Original Position: " + _positionAtStartOfRecord);

                //Need to display the value of a reference, so check if the reference "is valid" is necessary.
                if (LastDuplicatedElementsList.Count > 0 && Selection.activeGameObject == MainObjectOfSelection) {
                    //Display the value of the recorded movement, selectable in case of need for the user to store the value
                    EditorGUILayout.SelectableLabel("Current Position: " + MainObjectOfSelection.transform.position);
                }

                EditorGUILayout.SelectableLabel("Next call will record: " + _nextCallMustRecord);

            }
        }
        #endregion
    }

    /// <summary>
    /// Crate a copy of the selection, then start rcording translation.
    /// </summary>
    static void DuplicateThenStartRecord() {
        //Using Instanciate() with a GameObject will also instanciate all of its child and subchild, 
        //so any child must be removed from the selection.
        RemoveChildOfSelectedObjectFromSelection();
        Duplicate();
        //Set recording state as true.
        _isRecording = true;
        //Since recording started, next call must not record.
        _nextCallMustRecord = false;
        _positionAtStartOfRecord = CenterOfSelection;
    }



    /// <summary>
    /// Remove any game object from current Selection if its parent is in the current Selection.
    /// </summary>
    private static void RemoveChildOfSelectedObjectFromSelection() {


        //Get Transform of all Selected Game object in a temp list, with a for loop.
        List<Transform> allSelectedsTransform = new List<Transform>(Selection.gameObjects.Length);
        for (int i = 0; i < Selection.gameObjects.Length; i++) {
            allSelectedsTransform.Add(Selection.gameObjects[i].transform);
        }

        //Create a list of GameObject that will be the next Selection.
        List<GameObject> filteredGameObjects = new List<GameObject>(allSelectedsTransform.Count);
        //Populate it with a for loop.
        for (int i = 0; i < allSelectedsTransform.Count; i++) {

            //If parent is null, no need to check.
            if (allSelectedsTransform[i].transform.parent != null) {
                //If the list of transform contains the parent transfrom, skip this loop iteration.
                // "if(allSelectedsTransform.Contains(allSelectedsTransform[i].parent)" can't be used, in case of sub child.
                for (int j = 0; j < allSelectedsTransform.Count; j++) {
                    if (allSelectedsTransform[i].IsChildOf(allSelectedsTransform[j].transform) && i != j /* since IsChildOf return true when compared to itself, comparison with itself MUST be avoided */  ) {
                        Debug.LogFormat("A GameObject was removed from the Selection, because its parent was in it: {0}", allSelectedsTransform[i].name);
                        //Skip the current iteration.
                        continue;
                    }
                }
            }
            //If this interation was not skipped, then the GameObject is not a child
            filteredGameObjects.Add(allSelectedsTransform[i].gameObject);
        }
        //Set the new Selection without any child or subchild in it.
        Selection.objects = filteredGameObjects.ToArray();
    }

    /// <summary>
    /// Duplicate the selection, then apply the translation.
    /// </summary>
    static void DuplicateThenTranslate() {
        //Stop recording if still recording.
        _isRecording = false;

        Duplicate();


        //The commented code here was created in an attempt to support rotation record
        //While any code directly using rotation has been removed, the core of this attempt is kept
        //to not start from scratch.
        //The logic was to attach all object to a temp GameObject, placed at the center of the Selection,
        //then to rotate and move this game object, reassign the correct parents, then delete the temp GO.

        #region Legacy Code
        //Vector3 center = Vector3.zero;

        //for (int i = 0; i < LastDuplicatedElementsList.Count; i++) {
        //    center += LastDuplicatedElementsList[i].transform.position;
        //}

        //center /= LastDuplicatedElementsList.Count;


        //GameObject tempGO = new GameObject("temp");
        //tempGO.transform.eulerAngles = LastDuplicatedElementsList[0].transform.eulerAngles;
        //tempGO.transform.position = center;


        //List<Transform> originalsParentsStoredToReassignLater = new List<Transform>(LastDuplicatedElementsList.Count);


        //for (int i = 0; i < LastDuplicatedElementsList.Count; i++) {
        //    originalsParentsStoredToReassignLater.Add(LastDuplicatedElementsList[i].transform.parent);
        //    LastDuplicatedElementsList[i].transform.parent = tempGO.transform;
        //}

        //for (int i = 0; i < LastDuplicatedElementsList.Count; i++) {
        //    originalsParentsStoredToReassignLater.Add(LastDuplicatedElementsList[i].transform.parent);
        //    LastDuplicatedElementsList[i].transform.parent = tempGO.transform;
        //}


        //tempGO.transform.Translate(_translateToApply, Space.Self);

        //for (int i = 0; i < LastDuplicatedElementsList.Count; i++) {
        //    LastDuplicatedElementsList[i].transform.parent = originalsParentsStoredToReassignLater[i];
        //}

        //DestroyImmediate(tempGO);

        #endregion


        //For each duplicated element, move it with the recorded translate Vector3.
        for (int i = 0; i < LastDuplicatedElementsList.Count; i++) {
            LastDuplicatedElementsList[i].transform.Translate(_translateToApply, Space.Self);
        }

        //Repaint to be sure that duplicated elements are correctly drawed in the SceneView.
        SceneView.lastActiveSceneView.Repaint();
    }

    /// <summary>
    /// Duplicate the current selected GameObjects. Populate LastDuplicatedElementsList with the duplicates.
    /// </summary>
    static void Duplicate() {
        //Create a temp list.
        List<GameObject> newGameObjectListTemp = new List<GameObject>(Selection.gameObjects.Length);

        //Duplicate each gameobject of the selection.
        for (int i = 0; i < Selection.gameObjects.Length; i++) {
            //Try to get the prefab origin of the gameobject
            GameObject prefabRoot = PrefabUtility.GetCorrespondingObjectFromSource(Selection.gameObjects[i]);
            //create a reference for the new duplicated game object
            GameObject newGameObject;

            //If the game object was a prefab, prefabRoot is not null
            if (prefabRoot != null) {
                //Instanciate the prefab with the appropriate method.
                newGameObject = (GameObject)PrefabUtility.InstantiatePrefab(prefabRoot);
            }
            else //else the gameobject is just a simple scene object.
            {
                newGameObject = GameObject.Instantiate(Selection.gameObjects[i]);
            }

            //Set the position and rotations value even if the new gameobject was instantiated from a scene object.
            //When unity shortcut for duplication is used, sometime the position is slighty changed, so for precaution, it must be set again.

            //Set postion, rotation equal to original object
            newGameObject.transform.position = Selection.gameObjects[i].transform.position;
            newGameObject.transform.rotation = Selection.gameObjects[i].transform.rotation;

            //Assign parent before setting localScale
            newGameObject.transform.parent = Selection.gameObjects[i].transform.parent;

            //Set local scale
            newGameObject.transform.localScale = Selection.gameObjects[i].transform.localScale;

            //Duplicate the name (to avoid having object with many "(Clone)" in their name, for both readability of the Scene,
            //and because some script sometime check if name of a GameObject contain "(Clone)"
            newGameObject.transform.name = Selection.gameObjects[i].transform.name;
            newGameObject.transform.name = "Duplicated " + i;

            //Add this to the temp list of gameobject
            newGameObjectListTemp.Add(newGameObject);

            //Define the first duplicate element as the main gameobject
            if (i == 0) {
                Undo.RecordObject(window, "Changed MainObjectSelection");
                MainObjectOfSelection = newGameObject;
            }

            //For some reason, when duplicating multiple game object, Ctrl + Z undo the whole group, but since this is the 
            //wanted behaviour, there is no need to find why it doesn't record the undo as separates.
            Undo.RegisterCreatedObjectUndo(newGameObject, "Duplicated object, index from selection: " + i);

        }

        //Clear the list
        LastDuplicatedElementsList.Clear();
        //Copy the temp list
        LastDuplicatedElementsList = new List<GameObject>(newGameObjectListTemp);
        //Sort them by instance ID (used for a comparison later)
        LastDuplicatedElementsList.Sort((obj1, obj2) => obj1.GetInstanceID().CompareTo(obj2.GetInstanceID()));
        //set the new duplicated object as the selection
        Selection.objects = LastDuplicatedElementsList.ToArray();

    }

    //Called each time inspector update
    //Used to update displayed info in the windows every frame if the user changed something.
    void OnInspectorUpdate() {
        if (_isRecording) {
            //Translate value is updated only when recording, 
            //so in duplication phase, it is possible to move
            //the selection to use the same translate with a
            //different starting point.
            UpdateTranlateValue();
        }
        Repaint();
    }

    /// <summary>
    /// Enables the Editor to handle an event in the Scene view.
    /// </summary>
    public void OnSceneGUI(SceneView view) {
        Event e = Event.current;

        //switch because use of other event may be needed in a future update of the script
        switch (e.type) {
            //check if mouse button pressed
            case EventType.MouseDown:
                //check if left click
                if (e.button == 0) {
                    //Each time the left button is pressed, check if the selection is still the same,
                    //to differenciate selection movement with the anchor and unselection
                    if (!SelectionIsValid) {
                        //stop current recording
                        _isRecording = false;
                        //prepare next recording
                        _nextCallMustRecord = true;
                    }
                }
                break;
            case EventType.KeyDown:
                if (e.keyCode == KeyCode.Escape) {
                    _nextCallMustRecord = true;
                }
                break;
        }
    }

    /// <summary>
    /// Compare starting and current position of the selection, then update LastTranslate vector.
    /// </summary>
    static void UpdateTranlateValue() {
        if (MainObjectOfSelection) {
            _translateToApply = CenterOfSelection - _positionAtStartOfRecord;
        }
    }


    static Vector3 CenterOfSelection {
        get {
            Vector3 center = Vector3.zero;

            for (int i = 0; i < LastDuplicatedElementsList.Count; i++) {
                center += LastDuplicatedElementsList[i].transform.position;
            }

            center /= LastDuplicatedElementsList.Count;
            return center;
        }
    }

}
