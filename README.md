
## READ BEFORE USE

This C# script file is intended to use for **UnityEngine** projects,  
intended to be used when using **modular** level design prefabs, for example
like repeatedly placing wall meshes to build a corridor, or place identical
decorations at regular interval. 
  
Despite many tests, some edgecases may exist and cause damage to a Scene, so it  
is encouraged to work on copy of your work before using it.
  
**Undo** operations are **supported**.  
  
Rotations during record are not takn into account to apply the translation,  
therefore, **rotations are not supported**.

![example](images/DuplicateAndTranslate.gif "Example")
